/*
 * shared between pesi.h and node.h
 * because of (struct pesi).nodestock
 */

#ifndef PESI_NODE_HEAD_H
#define PESI_NODE_HEAD_H
struct node;
VSTAILQ_HEAD(node_head, node);
void node_init_nodestock(struct node_head *);
#endif
