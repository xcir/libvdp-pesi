/*-
 * Copyright 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * PESI per request state
 */

struct pesi_tree;

struct pesi * pesi_new(struct ws *ws, struct pesi_tree *pesi_tree);
void pesi_destroy(struct pesi **pesip);
void task_fini(struct pesi_tree *pesi_tree, struct pesi *pesi);

/*
 * per request state
 *
 * pecx: ESI parser
 * pesi: any request
 *
 */
struct pecx {
	unsigned		magic;
#define PECX_MAGIC		0x5d8cd06d
	const uint8_t		*p;
	const uint8_t		*e;

	ssize_t			l;
	int			state;
};

struct pesi {
	unsigned		magic;
#define PESI_MAGIC 0xa6ba54a0
	unsigned		flags;

	struct pesi_tree	*pesi_tree;
	struct worker		*wrk;
	struct node		*node;
	int			woken;

	struct pecx		pecx[1];
	VTAILQ_ENTRY(pesi)	list;
	struct node_head	nodestock;

	unsigned		no_thread;

#ifdef DEBUG_PESI_WS
	uintptr_t		ws_snap;
#endif
};
