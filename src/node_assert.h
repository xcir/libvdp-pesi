/*-
 * Copyright 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * tree integrity check
 *
 * CHK_ORDER: within child list can only transition from ST_DELIVERED -> !
 * ST_DELIVERED iow, once a child is found pending, all others must be also
 */

enum check_state {
	CHK_ANY = 0,
	CHK_PEND,	// ! ST_DELIVERED && ! ST_UNPENDING
	CHK_DELI,	// ST_DELIVERED
	CHK_ORDER
};

/*
 * keep in prod builds for now to gain trust in tree sanity
 *
 * XXX enable only for DEBUG later?
 */
#ifdef NO_ASSERT_NODE
#define assert_node(n, c) (void)0
#define assert_nexus(n, c) (void)0
#else
static inline void assert_node(const struct node *node, enum check_state check);

static inline void
assert_nexus(const struct node *node, enum check_state nexcheck)
{
	struct node *child;
	enum check_state check;

	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	assert(node->type == T_NEXUS);

	check = nexcheck;
	VSTAILQ_FOREACH(child, &node->nexus.children, sibling) {
		if (child->type == T_NEXUS) {
			/*
			 * actually, we would need to communicate up from the
			 * subtree the fact that any <= ST_CLOSED was found and
			 * set check accordingly for CHK_ORDER as below, but
			 * still this is only an assertion....
			 */
			assert_node(child, nexcheck);
			continue;
		}
		assert_node(child, check);
		if (check == CHK_ORDER && child->state <= ST_CLOSED)
			check = CHK_PEND;
	}
}

static inline void
assert_node(const struct node *node, enum check_state check)
{
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);

	switch (check) {
	case CHK_ANY:
	case CHK_ORDER:
		break;
	case CHK_PEND:
		// not ST_UNPENDING ST_DELIVERED ST_PRUNED
		assert(node->state <= ST_CLOSED);
		break;
	case CHK_DELI:
		if (node->state == ST_PRUNED)
			assert(node->type == T_NEXUS);
		else
			assert(node->state == ST_DELIVERED);
		break;
	default:
		INCOMPL();
	}

	switch (node->state) {
	case ST_PRIVATE:
	case ST_PRUNED:
		assert(node->type == T_NEXUS);
		/* hands off all nexus fields */
		break;
	case ST_DATA:
	case ST_UNPENDING:
		assert(node->type != T_NEXUS);
		break;
	case ST_OPEN:
		AN(node->nexus.owner);
		AZ(node->nexus.npending_private);
		assert_nexus(node, CHK_ORDER);
		break;
	case ST_CLOSED:
		AZ(node->nexus.owner);
		AZ(node->nexus.npending_private);
		assert_nexus(node, CHK_ORDER);
		break;
	case ST_DELIVERED:
		if (node->type == T_NEXUS) {
			assert_nexus(node, CHK_DELI);
			AZ(node->nexus.oc);
		} else if (node->type == T_DATA) {
			AZ(node->data.st);
		}
		break;
	default:
		INCOMPL();
	}
}
#endif
