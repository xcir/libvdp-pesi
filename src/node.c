/*-
 * Copyright 2019 - 2021 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Authors: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *	    Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * pesi nodes
 */

#include "config.h"

#include "cache/cache_varnishd.h"
#include "cache/cache_filter.h"
#include "cache/cache_objhead.h"
#include "foreign/from_cache_esi_deliver.h"
#include "storage/storage.h"
#include "vend.h"
#include "vgz.h"

#include "debug.h"
#include "node.h"
#include "node_assert.h"
#include "node_mempool.h"
#include "pesi.h"

#include "misc.h"

#include "vcc_pesi_if.h"

#define GZIP_TAILBUF_SZ 13

/* ============================================================
 * node finalizers
 */
static void fini_final(struct vdp_ctx *, struct node *);
static void fini_subreq(const struct vdp_ctx *, struct node *);
static void fini_data(struct vdp_ctx *, struct node *);

/* ============================================================
 * tuneables which we may or may not want to expose
*/

/*
 * nodes on the workspace:
 *
 * Because we ensure that we do not fini any ESI T_NEXUS request before all
 * children have been fini't (we need to do this anyway because all children
 * need the nexus' VDP chain), we can allocate any ESI T_NEXUS' children from
 * its workspace
 *
 * This is great, because it avoids using a central allocator, reduces memory
 * requirements and, in particular, allows for totally lock free allocations,
 * because we create all children of a nexus from its request context anyway (if
 * not, a node can still be requested from a different allocator like the
 * mempool).
 *
 * Other vmods could also require a foreign request's workspace (for all
 * practical purposes this will probably be the topreq's workspace for
 * PRIV_TOP).
 *
 * If we just accessed the workspace for every node allocation, this would
 * create more conflict points with such allocations (which need to be
 * synchronised to work with pesi anyway), so we better stay out of the way and
 * pre-allocate a sensible number of nodes. This also increases memory locality
 *
 * ws_min_room  is the minmum bytes we keep available,
 * ws_max_nodes is the maximum numer of nodes to allocate from the workspace
 *
 * data collection from our vtcs with DEBUG_PESI_WS enabled:
 * $ find . -name \*.log | xargs grep -h "ws used" | sort| uniq -c
 *       3 **** v1   vsl|          0 Debug           - ws used 104 free 56320
 *       3 **** v1   vsl|          0 Debug           - ws used 104 free 56464
 *       1 **** v1   vsl|          0 Debug           - ws used 104 free 56648
 *       1 **** v1   vsl|          0 Debug           - ws used 104 free 56672
 *      19 **** v1   vsl|          0 Debug           - ws used 104 free 56736
 *      16 **** v1   vsl|          0 Debug           - ws used 104 free 56752
 *       7 **** v1   vsl|          0 Debug           - ws used 104 free 56760
 *       4 **** v1   vsl|          0 Debug           - ws used 128 free 56688
 *       2 **** v1   vsl|          0 Debug           - ws used 128 free 56704
 *       2 **** v1   vsl|          0 Debug           - ws used 128 free 56712
 *       1 **** v1   vsl|          0 Debug           - ws used 64 free 56280
 *       1 **** v1   vsl|          0 Debug           - ws used 64 free 56296
 *       1 **** v1   vsl|          0 Debug           - ws used 64 free 56304
 *       7 **** v1   vsl|          0 Debug           - ws used 64 free 56344
 *       5 **** v1   vsl|          0 Debug           - ws used 64 free 56352
 *       3 **** v1   vsl|          0 Debug           - ws used 64 free 56360
 *      14 **** v1   vsl|          0 Debug           - ws used 64 free 56368
 *       2 **** v1   vsl|          0 Debug           - ws used 64 free 56376
 *       1 **** v1   vsl|          0 Debug           - ws used 64 free 56432
 *      15 **** v1   vsl|          0 Debug           - ws used 64 free 56496
 *       4 **** v1   vsl|          0 Debug           - ws used 64 free 56504
 *      50 **** v1   vsl|          0 Debug           - ws used 64 free 56512
 *      19 **** v1   vsl|          0 Debug           - ws used 64 free 56520
 *
 * $ find . -name \*.log | xargs grep -h "nodes" | sort | uniq -c
 *     125 **** v1   vsl|          0 Debug           - nodes used 0
 *       8 **** v1   vsl|          0 Debug           - nodes used 1
 *       8 **** v1   vsl|          0 Debug           - nodes used 10
 *       4 **** v1   vsl|          0 Debug           - nodes used 3
 *      36 **** v1   vsl|          0 Debug           - nodes used 5
 *
 */

// known minimum workspace left over for other vmods running during delivery
static volatile size_t   ws_min_room  = 4 * 1024;
// sensible maximum number of nodes to pre-allocate on the workspace
static volatile unsigned ws_max_nodes = 32;

/* ============================================================
 * node/tree alloc / fini / free
 */

void
node_init_nodestock(struct node_head *head)
{
	VSTAILQ_INIT(head);
}

void
node_fill_nodestock(struct ws *ws, struct node_head *head)
{
	struct node *node;
	unsigned spc, n;
	void *p;

	if (VSTAILQ_FIRST(head) != NULL)
		return;

	WS_Assert(ws);
	AN(head);

	spc = WS_ReserveAll(ws);
	p = WS_Reservation(ws);

	if (spc < ws_min_room)
		n = 0;
	else
		n = (spc - ws_min_room) / sizeof *node;

	if (n > ws_max_nodes)
		n = ws_max_nodes;

	spc = n * sizeof *node;

	WS_Release(ws, spc);

	if (spc == 0)
		return;

	memset(p, 0, spc);
	node = p;
	while (n--) {
		node->magic = NODE_MAGIC;
		node->allocator = NA_WS;
		VSTAILQ_INSERT_TAIL(head, node, sibling);
		node++;
	}
	assert((char *)node <= ws->f);
}

size_t
node_size()
{
	return (sizeof(struct node));
}

VCL_VOID
vmod_workspace_prealloc(VRT_CTX, VCL_BYTES min_free, VCL_INT max_nodes)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (max_nodes < 0) {
		VFAIL(ctx, "max_nodes (%jd) < 0 in workspace_prealloc()",
		      max_nodes);
		return;
	}
	ws_min_room = min_free;
	ws_max_nodes = max_nodes;
}

struct node *
node_alloc(struct pesi *pesi)
{
	struct node *node;
	unsigned sz;

	if (pesi != NULL &&
	    (node = VSTAILQ_FIRST(&pesi->nodestock)) != NULL) {
		VSTAILQ_REMOVE_HEAD(&pesi->nodestock, sibling);
		VSTAILQ_NEXT(node, sibling) = NULL;
		CHECK_OBJ(node, NODE_MAGIC);
		assert(node->allocator == NA_WS);
		return (node);
	}

	AN(mempool);
	node = MPL_Get(mempool, &sz);
	MPL_AssertSane(node);
	assert(sz >= sizeof *node);
	// MPL zeroes
	AZ(node->magic);
	node->magic = NODE_MAGIC;
	node->allocator = NA_MPL;
	return (node);
}

static void
node_fini(struct vdp_ctx *vdx, struct node *node)
{

	switch (node->type) {
	case T_FINAL:
		fini_final(vdx, node);
		break;
	case T_SUBREQ:
		fini_subreq(vdx, node);
		break;
	case T_DATA:
		fini_data(vdx, node);
		break;
	case T_CRC:
		break;
	case T_NEXUS:
		/* root req owned by varnish-cache */
		if (node->parent == NULL)
			break;
		assert(VSTAILQ_EMPTY(&node->nexus.children));
		req_fini(&node->nexus.req, vdx->wrk);
		break;
	default:
		INCOMPL();
	}
}

void
tree_prune(struct vdp_ctx *vdx, struct node *node)
{
	struct node *child;

	assert(node->type == T_NEXUS);

	while ((child = VSTAILQ_FIRST(&node->nexus.children)) != NULL) {
		VSTAILQ_REMOVE_HEAD(&node->nexus.children, sibling);
		tree_free(vdx, child);
	}

	node->state = ST_PRUNED;
}

/* recursively free all nodes == free the tree */
void
tree_free(struct vdp_ctx *vdx, struct node *node)
{
	if (node->type == T_NEXUS)
		tree_prune(vdx, node);

	node_fini(vdx, node);

	switch (node->allocator) {
	case NA_WS:
		return;
	case NA_MPL:
		AN(mempool);
		MPL_AssertSane(node);
		MPL_Free(mempool, node);
		return;
	default:
		INCOMPL();
	}
}

/* ============================================================
 * node insert / state transitions
 */

void
node_insert(struct bytes_tree *tree, struct node *parent,
    struct node *node)
{
	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(parent, NODE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);

	assert(parent->type == T_NEXUS);

	if (parent->state == ST_OPEN)
		Lck_Lock(&tree->tree_lock);
	else
		assert(parent->state == ST_PRIVATE);

	switch (node->type) {
	case T_NEXUS:
		assert(node->state == ST_PRIVATE ||
		       node->state == ST_OPEN);
		assert(node->nexus.gzip.magic == NEXUS_GZIP_MAGIC);
		break;
	case T_CRC:
	case T_DATA:
		assert(node->state == ST_DATA);
		break;
	case T_SUBREQ:
	case T_FINAL:
	default:
		/* cannot insert SUBREQ and FINAL yet */
		INCOMPL();
	}

	AZ(node->parent);

	node->parent = parent;
	if (node->type == T_NEXUS)
		VSTAILQ_INIT(&node->nexus.children);

	VSTAILQ_INSERT_TAIL(&parent->nexus.children, node, sibling);

	assert(tree->npending >= 0);

	if (parent->state == ST_PRIVATE) {
		parent->nexus.npending_private++;
	}
	else {
		assert(parent->state == ST_OPEN);
		AZ(parent->nexus.npending_private);
		tree->npending++;

		if (node->state == ST_DATA)
			AZ(pthread_cond_signal(&tree->cond));

		Lck_Unlock(&tree->tree_lock);
	}

	VSLdbgv(tree->root->nexus.req, "node_insert: node=%p parent=%p "
	    "parent->state=%d parent->npending_private=%d "
	    "npending=%d",
	    node, parent,
	    parent->state, parent->nexus.npending_private,
	    tree->npending);
}

static void
set_unpending(const struct bytes_tree *tree, struct node *node)
{
	struct node *parent;

	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	parent = node->parent;
	CHECK_OBJ_ORNULL(parent, NODE_MAGIC);

	Lck_AssertHeld(&tree->tree_lock);

	assert(node->state == ST_DATA);
	assert(node->type == T_DATA ||
	       node->type == T_SUBREQ ||
	       node->type == T_FINAL ||
	       node->type == T_CRC);

	node->state = ST_UNPENDING;

	assert_node(node, CHK_ANY);

	if (parent == NULL)
		return;

	assert(parent->state == ST_OPEN || parent->state == ST_CLOSED);

}

static void
set_delivered(struct bytes_tree *tree, struct node *node)
{
	struct node *parent;
	struct req *req;

	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	parent = node->parent;
	CHECK_OBJ_ORNULL(parent, NODE_MAGIC);

	Lck_AssertHeld(&tree->tree_lock);

	assert(node->state == ST_DATA ||
	       node->state == ST_CLOSED ||
	       node->state == ST_UNPENDING);

	if (node->type == T_NEXUS &&
	    node->nexus.oc != NULL) {
		req = tree->root->nexus.req;
		CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
		CHECK_OBJ_NOTNULL(req->wrk, WORKER_MAGIC);

		assert(node->state == ST_CLOSED);
		(void) HSH_DerefObjCore(req->wrk, &node->nexus.oc, 0);
	}

	node->state = ST_DELIVERED;
	assert(tree->npending > 0);
	tree->npending--;

	assert_node(node, CHK_DELI);

	if (parent == NULL) {
		/* Response has esi:remove but no esi:include. */
		AN(node);
		assert(node == tree->root);
		VSL0dbg("set_delivered: root node=%p", node);
		return;
	}

	assert(parent->state == ST_OPEN || parent->state == ST_CLOSED);

	if (parent->state == ST_OPEN || VSTAILQ_NEXT(node, sibling) != NULL) {
		VSL0dbg("set_delivered: stop parent=%p state=%d "
		    "sibling %p", parent, parent->state,
		    VSTAILQ_NEXT(node, sibling));
		return;
	}

	set_delivered(tree, parent);
}

/* ST_PRIVATE -> ST_OPEN transition
 *
 * child nodes may get unpended by owner
 * any access locked only
 */

void
set_open(struct bytes_tree *tree, struct node *node, const struct worker *wrk)
{
	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);

	Lck_AssertHeld(&tree->tree_lock);

	VSL0dbg("set_open node=%p wrk=%p", node, wrk);

	assert(node->state == ST_PRIVATE);
	AZ(node->nexus.owner);

	node->state = ST_OPEN;
	tree->npending += node->nexus.npending_private;
	node->nexus.npending_private = 0;
	node->nexus.owner = wrk;
}

/* (ST_PRIVATE | ST_OPEN) -> CLOSE transition
 *
 * possible states and transitions
 *
 * undelivered children: -> ST_CLOSED
 * all children delivered: -> ST_DELIVERED
 */

void
set_closed(struct bytes_tree *tree, struct node *node, const struct worker *wrk)
{
	struct node *child;

	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	Lck_AssertHeld(&tree->tree_lock);

	if (node->state == ST_PRIVATE)
		set_open(tree, node, wrk);

	VSL0dbg("set_closed node=%p wrk=%p", node, wrk);

	assert(node->state == ST_OPEN);
	assert(node->nexus.owner == wrk);
	assert_node(node, CHK_ORDER);

	node->state = ST_CLOSED;
	node->nexus.owner = NULL;

	/* unpending may wait for this node to close */
	AZ(pthread_cond_signal(&tree->cond));

	if (node->type == T_NEXUS)
		VSTAILQ_FOREACH(child, &node->nexus.children, sibling)
			if (child->state < ST_DELIVERED)
				return;

	set_delivered(tree, node);
}

/* ============================================================
 * misc/util
 *
 */

/*
 * restore the original subreq state and take care that we do not mess up flags,
 * objcore etc
*/
static struct req *
subreq_fixup_oc(struct node *node)
{
	struct req *subreq;

	subreq = node->subreq.req;
	CHECK_OBJ_NOTNULL(subreq, REQ_MAGIC);

	/*
	 * we want to make sure there is only ever one reference to the objcore.
	 * as we get called from push_subreq() and fini_subreq(), depending on
	 * if push_subreq() has already been called, the transfer has already
	 * happened or not
	 */

	if (subreq->objcore != NULL) {
		CHECK_OBJ_NOTNULL(subreq->objcore, OBJCORE_MAGIC);
		AZ(node->subreq.oc);
	}
	else {
		TAKE_OBJ_NOTNULL(subreq->objcore, &node->subreq.oc,
		    OBJCORE_MAGIC);
	}

	return (subreq);
}

static struct req *
subreq_fixup(struct node *node, struct req *req)
{
	struct req *subreq;

	CHECK_OBJ_NOTNULL(node, NODE_MAGIC);
	assert(node->type == T_SUBREQ);
	AN(node->subreq.done);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);

	subreq = subreq_fixup_oc(node);

	if (subreq->wrk != NULL)
		assert(subreq->wrk == req->wrk);
	else
		subreq->wrk = req->wrk;

	if (subreq->top->topreq != NULL)
		assert(subreq->top->topreq == req);
	else
		subreq->top->topreq = req;

	return (subreq);
}

/* ------------------------------------------------------------
 * node cleanup (not holding tree lock)
 *
 * must be idempotent
 */
static void
fini_final(struct vdp_ctx *vdx, struct node *node)
{
	(void) vdx;

	assert(node->type == T_FINAL);

	if (node->final.fi_state == FI_DESTROYED)
		return;

	AZ(pthread_mutex_lock(&node->final.fi_mtx));

	if (node->final.fi_state < FI_GO) {
		node->final.fi_state = FI_GO;
		AZ(pthread_cond_signal(&node->final.fi_cond));
	}

	if (node->final.fi_state < FI_DONE)
		AZ(pthread_cond_wait(&node->final.fi_cond,
				     &node->final.fi_mtx));

	AZ(pthread_mutex_unlock(&node->final.fi_mtx));

	assert(node->final.fi_state == FI_DONE);

	AZ(pthread_cond_destroy(&node->final.fi_cond));
	AZ(pthread_mutex_destroy(&node->final.fi_mtx));

	node->final.fi_state = FI_DESTROYED;
}

static void
fini_subreq(const struct vdp_ctx *vdx, struct node *node)
{
	struct req *subreq;
	struct boc *boc = NULL;
	struct pesi *pesi;

	assert(node->type == T_SUBREQ);

	AN(node->subreq.done);

	if (node->subreq.req == NULL) {
		AZ(node->subreq.boc);
		AZ(node->subreq.oc);

		return;
	}

	(void) subreq_fixup_oc(node);

	TAKE_OBJ_NOTNULL(subreq, &node->subreq.req, REQ_MAGIC);
	if (node->subreq.boc)
		TAKE_OBJ_NOTNULL(boc, &node->subreq.boc, BOC_MAGIC);
	AZ(node->subreq.oc);

	// need before pesi_destroy
	(void) VDP_Close(subreq->vdc);

	/* bottom of cnt_transmit() */
	HSH_Cancel(vdx->wrk, subreq->objcore, boc);

	if (boc != NULL)
		HSH_DerefBoc(vdx->wrk, subreq->objcore);

	(void)HSH_DerefObjCore(vdx->wrk, &subreq->objcore, 0);

	TAKE_OBJ_NOTNULL(pesi, &subreq->transport_priv, PESI_MAGIC);
	pesi_destroy(&pesi);
	req_fini(&subreq, vdx->wrk);

	AZ(pthread_cond_destroy(&node->subreq.cond));
}

static void
fini_data(struct vdp_ctx *vdx, struct node *node)
{
	(void) vdx;

	assert(node->type == T_DATA);

	if (node->data.st == NULL)
		return;

	stv_transient->sml_free(node->data.st);
	node->data.st = NULL;
}

/* ============================================================
 * pushing nodes' data up
 * (unlocked)
 */

static int
node_bytes(struct vdp_ctx *vdx, struct bytes_tree *tree,
    const struct node *node, enum vdp_action act, const void *ptr, ssize_t sz)
{
	if (node->is_end) {
		AZ(tree->end_sent);
		assert(tree->end == node);
		tree->end_sent = 1;
		act = VDP_END;
	} else if (act == VDP_END) {
		act = VDP_FLUSH;
	}

	return (VDP_bytes(vdx, act, ptr, sz));
}

static int
push_final(struct vdp_ctx *vdx, struct bytes_tree *tree,
    struct node *node, const struct node *next)
{
	(void) vdx;
	(void) tree;
	(void) next;

	assert(node->type == T_FINAL);
	assert(node->final.fi_state == FI_READY);

	AZ(pthread_mutex_lock(&node->final.fi_mtx));

	node->final.fi_state = FI_GO;
	AZ(pthread_cond_signal(&node->final.fi_cond));

	if (node->final.fi_state < FI_DONE)
		AZ(pthread_cond_wait(&node->final.fi_cond,
				     &node->final.fi_mtx));

	AZ(pthread_mutex_unlock(&node->final.fi_mtx));

	assert(node->final.fi_state == FI_DONE);
	return (tree->retval);
}

static int
push_subreq(struct req *req, struct bytes_tree *tree,
    struct node *node, const struct node *next)
{
	struct req *subreq;

	assert(node->type == T_SUBREQ);
	(void) next;

	if (node->subreq.done == 0) {
		Lck_Lock(&tree->tree_lock);
		if (node->subreq.done == 0)
			AZ(Lck_CondWait(
				   &node->subreq.cond,
				   &tree->tree_lock, 0));
		Lck_Unlock(&tree->tree_lock);
	}

	subreq = subreq_fixup(node, req);

	AZ(subreq->objcore->flags & OC_F_FINAL);

	VSLdbg(vdx, "DeliverObj from top");
	/*
	 * XXX how should we handle a failed subreq?
	 * https://github.com/varnishcache/varnish-cache/issues/3264
	 */
	(void) VDP_DeliverObj(subreq->vdc, subreq->objcore);
	subreq->acct.resp_bodybytes += VDP_Close(subreq->vdc);

	// fini_subreq() happening later

	// tree->retval is sort-of inside out here, but
	// that detail really should not matter
	return (tree->retval);
}

static const uint8_t gzip_hdr[] = {
	0x1f, 0x8b, 0x08,
	0x00, 0x00, 0x00, 0x00,
	0x00,
	0x02, 0x03
};

static void
gzip_tailbuf(uint8_t tailbuf[GZIP_TAILBUF_SZ], const struct nexus_gzip *gz)
{
	AN(gz->is);
	AZ(gz->up);

	tailbuf[0] = 0x01;
	tailbuf[1] = 0x00;
	tailbuf[2] = 0x00;
	tailbuf[3] = 0xff;
	tailbuf[4] = 0xff;

	/* Emit CRC32 */
	vle32enc(tailbuf + 5, gz->crc);

	/* MOD(2^32) length */
	vle32enc(tailbuf + 9, gz->l_crc);
}

static int
push_crc(struct vdp_ctx *vdx, struct bytes_tree *tree,
    const struct node *node, const struct node *next)
{
	struct node *nex;
	struct nexus_gzip *parent_gzip;
	uint8_t tailbuf[GZIP_TAILBUF_SZ];

	assert(node->type == T_CRC);

	(void) vdx;

	nex = node->parent;
	CHECK_OBJ_NOTNULL(nex, NODE_MAGIC);
	assert(nex->type == T_NEXUS);
	AN(nex->nexus.gzip.is);

	switch(node->crc.ctype) {
	case GZIP_HDR:
		return (VDP_bytes(nex->nexus.req->vdc,
		    VDP_NULL, gzip_hdr, sizeof gzip_hdr));
	case UPDATE:
		nex->nexus.gzip.crc = crc32_combine(
			nex->nexus.gzip.crc, node->crc.icrc,
			node->crc.l_icrc);
		nex->nexus.gzip.l_crc += node->crc.l_icrc;
		break;
	case FINAL:
		parent_gzip = nex->nexus.gzip.up;

		if (next)
			assert(next->parent != nex);
		if (parent_gzip) {
			parent_gzip->crc = crc32_combine(
				parent_gzip->crc, nex->nexus.gzip.crc,
				nex->nexus.gzip.l_crc);
			parent_gzip->l_crc += nex->nexus.gzip.l_crc;
		} else {
			gzip_tailbuf(tailbuf, &nex->nexus.gzip);
			return (node_bytes(nex->nexus.req->vdc,
			    tree, node, VDP_FLUSH,
			    tailbuf, sizeof(tailbuf)));
		}
		break;
	default:
		INCOMPL();
	}

	return (0);
}


static int
push_data(struct vdp_ctx *vdx, struct bytes_tree *tree,
    const struct node *node, const struct node *next)
{
	const void *p;
	enum vdp_action act;

	assert(node->type == T_DATA);

	AN(node->parent);
	assert(node->parent->type == T_NEXUS);

	/*
	 * we ignore any act from the node and push all we got
	 * with VDP_FLUSH at the last node
	 */
	act = (next == NULL) ? VDP_FLUSH : VDP_NULL;

	p = node->data.ptr;
	if (p == NULL && node->data.len > 0) {
		CHECK_OBJ_NOTNULL(node->data.st, STORAGE_MAGIC);
		p = node->data.st->ptr;
	}

	if (p == NULL || node->data.len == 0)
		return (0);

	vdx = node->parent->nexus.req->vdc;
	return (node_bytes(vdx, tree, node, act, p, node->data.len));
}

/*
 * deliver unpended nodes while not holding the tree lock
 *
 * returns VDP_bytes upon any error
 */
static int
worklist_push(struct vdp_ctx *vdx, struct bytes_tree *tree,
    const struct node_head *work)
{
	struct node *node, *next, *parent;
	int retval = 0;

	node = VSTAILQ_FIRST(work);
	AN(node);
	while (node) {
		parent = node->parent;
		AN(parent);
		next = VSTAILQ_NEXT(node, unpend);

		assert (parent->type == T_NEXUS);
		if (parent->nexus.req->wrk == NULL)
			parent->nexus.req->wrk = vdx->wrk;
		else
			assert (parent->nexus.req->wrk == vdx->wrk);

		// not using assert_node: it needs locking
		assert(node->state == ST_UNPENDING);

		switch (node->type) {
		case T_FINAL:
			retval = push_final(vdx, tree, node, next);
			break;
		case T_SUBREQ:
			AN(tree->root);
			retval = push_subreq(tree->root->nexus.req, tree, node, next);
			break;
		case T_DATA:
			retval = push_data(vdx, tree, node, next);
			break;
		case T_CRC:
			retval = push_crc(vdx, tree, node, next);
			break;
		default:
			INCOMPL();
		}

		if (retval)
			break;

		if (VSTAILQ_NEXT(node, sibling) == NULL) {
			if (parent->parent == NULL)
				(void) 0;
			else if (parent->state < ST_CLOSED)
				assert(next == NULL);
			else
				parent->nexus.req->acct.resp_bodybytes +=
					VDP_Close(parent->nexus.req->vdc);
		}
		node = next;
	}

	if (retval) {
		(void) VDP_bytes(vdx, VDP_FLUSH, NULL, 0);
		return (retval);
	}

	/* not running the finis in the error case, they
	 * will be called from final cleanup via tree_free()
	 *
	 * this avoids waiting for subreq tasks
	 */

	VSTAILQ_FOREACH(node, work, unpend) {
		assert(node->type != T_NEXUS);
		node_fini(vdx, node);
	}

	return (retval);
}

static void
worklist_set_delivered(struct vdp_ctx *vdx, struct bytes_tree *tree,
    const struct node_head *work)
{
	struct node *node, *parent, *tmp;

	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	Lck_AssertHeld(&tree->tree_lock);

	VSTAILQ_FOREACH_SAFE(node, work, unpend, tmp) {
		set_delivered(tree, node);

		if (VSTAILQ_NEXT(node, sibling) != NULL)
			continue;

		parent = node->parent;

		if (parent->state < ST_CLOSED)
			continue;

		assert(parent->state == ST_DELIVERED);
		parent->state = ST_PRUNED;
		Lck_Unlock(&tree->tree_lock);

		tree_prune(vdx, parent);
		node_fini(vdx, parent);

		Lck_Lock(&tree->tree_lock);
	}
}

/*
 * to minimize lock contention, we walk the tree and gather work on a list
 */

#define set_front(tree, node, wrk) do {					\
		(tree)->front = node;					\
		(tree)->front_owner = wrk;				\
	} while(0)

#define set_end(tree, node) (tree)->end = node;

static void
worklist_unpend(const struct vdp_ctx *vdx, struct bytes_tree *tree,
    struct node_head *work)
{
	struct node *node, *next;
	enum check_state check = CHK_DELI;

	(void) check;	// for NODEBUG

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(tree, BYTES_TREE_MAGIC);
	AN(work);
	assert(VSTAILQ_EMPTY(work));

	Lck_AssertHeld(&tree->tree_lock);
	VSLdbgv(vdx->vsl, "bytes_unpend: retval=%d front=%p root=%p",
		tree->retval, tree->front, tree->root);
	if (tree->retval)
		return;

	CHECK_OBJ_NOTNULL(tree->front, NODE_MAGIC);
	CHECK_OBJ_NOTNULL(tree->root, NODE_MAGIC);

	if (tree->root->state == ST_DELIVERED ||
	    tree->root->state == ST_PRUNED) {
		VSLdbg(vdx, "bytes_unpend: whole tree is delivered");
		assert_node(tree->root, CHK_DELI);
		return;
	}

	/*
	 * if unpending changes between threads, we need to make sure that we
	 * only descend into ST_OPEN T_NEXUS which we own, so re-start at the
	 * root
	 *
	 * this might seem inefficient, but we skip over any delivered subtrees,
	 * so we will not walk the whole tree each time
	 */
	AN(tree->front);

	if (tree->front_owner == vdx->wrk)
		node = tree->front;
	else
		node = tree->root;

	/* when starting, we do not want to see any ST_UNPENDING nodes */
	check = CHK_DELI;

	while (node != NULL) {
		assert_node(node, CHK_ANY);

		while (node->state >= ST_UNPENDING) {
			assert_node(node, check);
			VSLdbgv(vdx, "bytes_unpend: delivered/unpending "
				"node %p state %u", node, node->state);

			next = VSTAILQ_NEXT(node, sibling);
			if (next != NULL) {
				node = next;
				continue;
			}

			if (node->parent && node->parent->state == ST_OPEN) {
				VSLdbgv(vdx, "bytes_unpend: return at "
					"node %p state %u - await sibling",
					node, node->state);
				set_front(tree, node, vdx->wrk);
				return;
			}

			/* up and right */
			node = node->parent;
			while (node != NULL) {
				assert(node->state != ST_PRUNED);
				next = VSTAILQ_NEXT(node, sibling);
				if (next != NULL) {
					node = next;
					break;
				}
				node = node->parent;
			}

			if (node != NULL)
				continue;

			VSLdbg(vdx, "bytes_unpend: reached root");
			set_front(tree, tree->root, vdx->wrk);
			if (tree->end != NULL)
				tree->end->is_end = 1;
			return;
		}

		if (node->state == ST_PRIVATE) {
			VSLdbgv(vdx, "bytes_unpend: return at "
				"node %p state %u - private",
				node, node->state);
			set_front(tree, node, vdx->wrk);
			return;
		}

		assert_node(node, CHK_PEND);

		/* for ST_OPEN, only descend if we own it */
		if (node->type == T_NEXUS) {
			next = VSTAILQ_FIRST(&node->nexus.children);

			switch (node->state) {
			case ST_CLOSED:
				// closed node with no children must not exist
				AN(next);
				node = next;
				continue;
			case ST_OPEN:
				set_front(tree, node, vdx->wrk);
				AN(node->nexus.owner);
				if (node->nexus.owner == vdx->wrk) {
					node = next;
					continue;
				}
				return;
			default:
				INCOMPL();
			}
		}

		assert(node->state == ST_DATA);
		assert(node->type > T_NEXUS);

		VSTAILQ_INSERT_TAIL(work, node, unpend);
		set_unpending(tree, node);
		set_end(tree, node);

		// for more iterations, also accept UNPEND
		// for now, we use any
		check = CHK_ANY;
	}
}
#undef set_front

void
tree_deliver(struct vdp_ctx *vdx, struct bytes_tree *tree)
{
	struct node_head work;
	int retval;

	CHECK_OBJ_NOTNULL(vdx, VDP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vdx->wrk, WORKER_MAGIC);

	if (tree->unpend_owner != NULL)
		return;

	tree->unpend_owner = vdx->wrk;

	/*lint -e{506} constant value boolean */
	do {
		Lck_AssertHeld(&tree->tree_lock);

		assert(tree->unpend_owner == vdx->wrk);

		VSTAILQ_INIT(&work);

		worklist_unpend(vdx, tree, &work);
		if (VSTAILQ_EMPTY(&work) || tree->retval)
			break;

		Lck_Unlock(&tree->tree_lock);
		retval = worklist_push(vdx, tree, &work);
		Lck_Lock(&tree->tree_lock);

		if (retval) {
			tree->retval = retval;
			break;
		}
		worklist_set_delivered(vdx, tree, &work);
	} while (1);

	tree->unpend_owner = NULL;
}
