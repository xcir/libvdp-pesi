varnishtest "Test all 8 gzip stopbit residuals"

server s1 {
	rxreq
	expect req.http.accept-encoding == gzip
	txresp -gzipbody {
		<html>					1
		Before includes				2
		{<esi:include src="/bit0"/>} bit0	3
		{<esi:include src="/bit1"/>} bit1	4
		{<esi:include src="/bit2"/>} bit2	5
		{<esi:include src="/bit3"/>} bit3	6
		{<esi:include src="/bit4"/>} bit4	7
		{<esi:include src="/bit5"/>} bit5	8
		{<esi:include src="/bit6"/>} bit6	9
		{<esi:include src="/bit7"/>} bit7	10
		After includes				11
	}
} -start

server s2 {
	rxreq
	expect req.url == "/bit0"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 0 -gziplevel 9 -gzipbody {e04c8d0fd604c}
} -start

server s3 {
	rxreq
	expect req.url == "/bit1"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 1 -gziplevel 9 -gzipbody {1ea86e6cf31bf4ec3d7a86}
} -start

server s4 {
	rxreq
	expect req.url == "/bit2"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 2 -gziplevel 9 -gzipbody {10}
} -start

server s5 {
	rxreq
	expect req.url == "/bit3"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 3 -gziplevel 9 -gzipbody {a5e2e2e1c2e2}
} -start

server s6 {
	rxreq
	expect req.url == "/bit4"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 4 -gziplevel 9 -gzipbody {71c5d18ec5d5d1}
} -start

server s7 {
	rxreq
	expect req.url == "/bit5"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 5 -gziplevel 9 -gzipbody {39886d28a6d2988}
} -start

server s8 {
	rxreq
	expect req.url == "/bit6"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 6 -gziplevel 9 -gzipbody {80000}
} -start

server s9 {
	rxreq
	expect req.url == "/bit7"
	expect req.http.accept-encoding == gzip
	txresp -gzipresidual 7 -gziplevel 9 -gzipbody {386811868}
} -start

varnish v1 -arg "-p thread_pool_min=100" -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_response {
		if (bereq.url == "/") {
			set beresp.do_esi = true;
		}
	}

	sub vcl_backend_fetch {
		if (bereq.url == "/") {
			set bereq.backend = s1;
		}
		elsif (bereq.url == "/bit0") {
			set bereq.backend = s2;
		}
		elsif (bereq.url == "/bit1") {
			set bereq.backend = s3;
		}
		elsif (bereq.url == "/bit2") {
			set bereq.backend = s4;
		}
		elsif (bereq.url == "/bit3") {
			set bereq.backend = s5;
		}
		elsif (bereq.url == "/bit4") {
			set bereq.backend = s6;
		}
		elsif (bereq.url == "/bit5") {
			set bereq.backend = s7;
		}
		elsif (bereq.url == "/bit6") {
			set bereq.backend = s8;
		}
		elsif (bereq.url == "/bit7") {
			set bereq.backend = s9;
		}
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

varnish v1 -cliok "param.set http_gzip_support true"
varnish v1 -cliok "param.set debug +esi_chop"
varnish v1 -cliok "param.set debug +syncvsl"

client c1 {
	txreq -hdr "Accept-Encoding: gzip"
	rxresp
	expect resp.http.content-encoding == gzip
	gunzip
	expect resp.status == 200
	expect resp.bodylen == 252

	txreq
	rxresp
	expect resp.http.content-encoding == <undef>
	expect resp.status == 200
	expect resp.bodylen == 252
} -run

varnish v1 -expect esi_errors == 0

## HTTP/2

varnish v1 -cliok "param.set feature +http2"

## See comments in e23.vtc about limitations using vtc to test
## gzipped H2 responses.

client c1 {
	stream 1 {
		txreq -hdr accept-encoding gzip
		rxresp
		expect resp.http.content-encoding == gzip
		expect resp.status == 200
	} -run
} -run
