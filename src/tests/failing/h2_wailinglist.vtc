varnishtest "waitinglists at multiple ESI depths"

barrier b1 cond 2

server s1 {
	rxreq
	txresp -body {<esi:include src="someurl"/>}
	rxreq
	txresp -body {<esi:include src="otherurl"/>}
	rxreq
	barrier b1 sync
	delay 1
	txresp -body {DATA}
} -start

varnish v1 -arg "-p debug=+waitinglist" -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_response {
		set beresp.do_esi = true;
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.body == "DATA"
} -start

client c2 {
	txreq
	barrier b1 sync
	rxresp
	expect resp.body == "DATA"
} -run

client c1 {
	txreq
	rxresp
	expect resp.body == "DATA"
} -run

## HTTP/2

varnish v1 -stop

barrier b2 cond 2

server s2 {
	rxreq
	txresp -body {<esi:include src="someurl"/>}
} -start

server s3 {
	rxreq
	txresp -body {<esi:include src="otherurl"/>}
} -start

server s4 {
	rxreq
	barrier b2 sync
	delay 1
	txresp -body {DATA}
} -start

varnish v2 -arg "-p debug=+waitinglist -p feature=+http2" -vcl+backend {
	import ${vmod_pesi};
	import ${vmod_pesi_debug};
	include "debug.inc.vcl";

	sub vcl_backend_fetch {
		if (bereq.url == "/") {
			set bereq.backend = s2;
		}
		elsif (bereq.url == "someurl") {
			set bereq.backend = s3;
		}
		else {
			set bereq.backend = s4;
		}
	}

	sub vcl_backend_response {
		set beresp.do_esi = true;
	}

	sub vcl_deliver {
		pesi.activate();
	}
} -start

client c1 -connect "${v2_sock}" {
	stream 1 {
		txreq
		rxresp
		expect resp.status == 200
		expect resp.body == "DATA"
	} -start
	stream 3 {
		txreq
		barrier b2 sync
		rxresp
		expect resp.status == 200
		expect resp.body == "DATA"
	} -run
	stream 5 {
		txreq
		rxresp
		expect resp.status == 200
		expect resp.body == "DATA"
	} -run
	stream 1 -wait
} -run
